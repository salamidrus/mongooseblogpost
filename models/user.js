const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new mongoose.Schema({
  username: String,
  posts: [{
    type: Schema.Types.ObjectId,
    ref: 'Post'
  }]
})

module.exports = mongoose.model('User', UserSchema);


